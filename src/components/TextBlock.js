import React from 'react'
import ReactDOM from 'react-dom'
import { Rnd } from 'react-rnd'
import { connect } from 'react-redux'
import { actions } from '../actions/blocks'
import { Editor, EditorState, RichUtils } from 'draft-js'
import EditableStyler from './EditableStyler'
import { customStyles, defaultStyles } from '../utils/styles'
import { withImmutable } from '../hoc'
// import { ContextMenu, MenuItem, ContextMenuTrigger } from 'react-contextmenu'
import _ from 'lodash'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

class Editable extends React.Component {
    // state = {
    //     editorState: EditorState.createEmpty()
    // }

    componentDidMount() {
        this.refs.editor.focus()
    }

    // onChange = (editorState, cb) => {
    //     this.setState({ editorState }, () => cb && cb())
    // }

    hasStyle = (inlineStyle) => {
        return this.props.editorState.getCurrentInlineStyle().has(inlineStyle)
    }

    getFontSize = () => {
        for (let customStyle of customStyles.styles)
            if (customStyle.style.includes('FONT_SIZE_') && this.hasStyle(customStyle.style)) return customStyle.value
    }

    updateStyles = () => {
        if (!this.props.inlineStyles) return {}
        let newInlineStyles = this.props.copy(this.props.inlineStyles)
        newInlineStyles['FONT_SIZE'] = this.getFontSize()
        for (let defStyle of defaultStyles) newInlineStyles[defStyle.style] = this.hasStyle(defStyle.style)
        if (!_.isEqual(this.props.inlineStyles, newInlineStyles)) return newInlineStyles
        return null
    }

    componentDidUpdate() {
        if (this.props.selected) {
            let newInlineStyles = this.updateStyles()
            if (newInlineStyles) this.props.setInlineStyles(newInlineStyles)
        }
        if (!this.props.readOnly) this.refs.editor.focus()
        if (this.props.inlineStyle && this.props.selectedBlock.id === this.props.uniqueId) {
            this.props.onChange(RichUtils.toggleInlineStyle(this.props.editorState, this.props.inlineStyle))
            this.props.toggleInlineStyle(null)
        }
    }

    render() {
        return (
            <Editor
                editorState={this.props.editorState}
                onChange={this.props.onChange}
                ref='editor'
                readOnly={this.props.readOnly}
                customStyleMap={customStyles.styleMap}
            />
        )
    }
}

class TextBlock extends React.Component {
    state = {
        clicked: false,
        doubleClicked: false,
        modified: false,
        editorState: EditorState.createEmpty(),
        x: (this.props.position && this.props.position.x) || 0,
        y: (this.props.position && this.props.position.y) || 0,
        height: (this.props.size && this.props.size.height) || '50px',
        width: (this.props.size && this.props.size.width) || '300px'
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickSomewhere)
        // this.onChange()
    }

    handleClickSomewhere = (e) => {
        const selfRef = ReactDOM.findDOMNode(this.selfRef), formatterRef = ReactDOM.findDOMNode(this.props.formatterRef)
        if ((selfRef && selfRef.contains(e.target)) || (formatterRef && formatterRef.contains(e.target))) {
            //smth
        } else {
            this.props.onClickOutside()
            this.setState({ clicked: false, doubleClicked: false })
            this.props.setInlineStyles({})
        }
    }

    setContainerStyle() {
        if (this.state.doubleClicked) return styles.editor
        if (this.state.clicked) return { ...styles.editor, cursor: 'move' }
        return { ...styles.editor, borderStyle: 'dotted', cursor: 'move' }
    }

    rndProps() {
        let props = {
            onClick: () => {
                if (this.state.modified) this.setState({ modified: false, clicked: true, doubleClicked: false })
                else this.setState({ doubleClicked: this.state.clicked ? true : false, clicked: true })
                this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'text' })
                this.props.onBlockClick()
            },
            disableDragging: this.state.doubleClicked,
            size: { width: this.state.width, height: this.state.height },
            position: { x: this.state.x, y: this.state.y }
        }
        if (this.state.doubleClicked) props.enableResizing = {}
        if (!this.state.doubleClicked) {
            props.onDrag = () => {
                if (!this.state.modified) {
                    if (!this.state.clicked) this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'text' })
                    this.setState({ modified: true, clicked: true })
                }
            }
            props.onResize = (e, direction, ref, delta, position) => {
                if (!this.state.modified) {
                    if (!this.state.clicked) this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'text' })
                    this.setState({ modified: true, clicked: true })
                }
                this.setState({
                    width: ref.style.width,
                    height: ref.style.height,
                    ...position,
                })
            }
            props.onDragStop = (e, d) => { this.setState({ x: d.x, y: d.y }, () => this.onChange()) }
            props.onResizeStop = () => this.onChange()
        } else {
            props.onResizeStart = () => false
            props.onDragStart = () => false
        }
        return props
    }

    onChange = () => {
        const { editorState, x, y, width, height } = this.state
        let blockState = {
            id: this.props.uniqueId,
            section: this.props.section,
            editorState,
            position: { x, y },
            size: { width, height }
        }
        console.log('newData', blockState)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickSomewhere)
    }

    render() {
        return (
            <div onContextMenu={(e) => {
                e.preventDefault()
            }}>
                <Rnd
                    ref={ref => this.selfRef = ref}
                    tabIndex='0'
                    style={this.setContainerStyle()}
                    default={{
                        x: 0,
                        y: 0
                    }}
                    {...this.rndProps()}
                >
                    {this.state.clicked && !this.state.doubleClicked && <EditableStyler />}
                    <div style={{ paddingLeft: 20, paddingRight: 20 }}>
                        <Editable
                            {...this.props}
                            uniqueId={this.props.uniqueId}
                            readOnly={!this.state.doubleClicked}
                            selected={this.state.clicked}
                            editorState={this.state.editorState}
                            onChange={(editorState) => this.setState({ editorState }, () => this.onChange())}
                        />
                    </div>
                </Rnd>
            </div>
        )
    }
}

const styles = {
    editor: {
        borderStyle: 'solid',
        borderWidth: '2px',
        borderColor: '#1B7EAB',
        outline: 'none'
    }
}

const mapStateToProps = (state) => ({
    inlineStyle: state.blocks.inlineStyle,
    selectedBlock: state.blocks.selectedBlock,
    formatterRef: state.blocks.formatterRef,
    inlineStyles: state.blocks.inlineStyles
})

const mapDispatchToProps = {
    setSelectedBlock: actions.setSelectedBlock,
    toggleInlineStyle: actions.toggleInlineStyle,
    setInlineStyles: actions.setInlineStyles
}

export default withImmutable(connect(mapStateToProps, mapDispatchToProps)(TextBlock))
