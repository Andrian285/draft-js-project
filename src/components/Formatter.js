import React, { Component } from 'react'
import { withStyles, Button } from '@material-ui/core'
import { connect } from 'react-redux'
import { actions } from '../actions/blocks'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import NativeSelect from '@material-ui/core/NativeSelect'
import Input from '@material-ui/core/Input'
import { SketchPicker } from 'react-color'
import { defaultStyles as formatters, customStyles } from '../utils/styles'
import '../assets/icon/style.css'

const OutsideButtons = (props) => (
    <div>
        <Button
            variant='contained'
            onClick={() => props.onAddBlock({ section: props.activeSection, type: 'text' })}
            className={props.classes.addTextBlock}
        >
            add text block
        </Button>
        <Button
            variant='contained'
            onClick={() => props.onAddBlock({ section: props.activeSection, type: 'shape' })}
            className={props.classes.addShape}
        >
            add shape
        </Button>
    </div>
)

class TextFormatter extends Component {
    toggleInlineStyle = (style) => {
        if (!this.props.selectedBlock) return
        this.props.toggleInlineStyle(style)
    }

    render() {
        const classes = this.props.classes
        return (
            <div>
                <div>
                    {formatters.map(formatter =>
                        <Button
                            variant='contained'
                            onClick={() => this.toggleInlineStyle(formatter.style)}
                            style={this.props.inlineStyles && this.props.inlineStyles[formatter.style] ?
                                { ...styles.button, backgroundColor: '#4D4D4D' } : styles.button}
                            color='secondary'
                            key={formatter.style}
                        ><span className={formatter.icon}></span></Button>
                    )}
                </div>
                <FormControl className={classes.formControl}>
                    <InputLabel shrink htmlFor='font-size-native-label-placeholder' style={styles.fontSizeLabel}>
                        Font size
                        </InputLabel>
                    <NativeSelect
                        value={'FONT_SIZE_' + ((this.props.inlineStyles && this.props.inlineStyles['FONT_SIZE']) || 16)}
                        onChange={(e) => this.toggleInlineStyle(e.target.value)}
                        input={<Input name='fontSize' id='font-size-native-label-placeholder' />}
                    >
                        {customStyles.styles.map(customStyle => customStyle.style.includes('FONT_SIZE_') &&
                            <option value={customStyle.style} key={customStyle.value}>{customStyle.value}</option>
                        )}
                    </NativeSelect>
                </FormControl>
            </div>
        )
    }
}

class ShapeFormatter extends React.Component {
    render() {
        const classes = this.props.classes
        return (
            <div className={classes.colorPickerContainer}>
                <SketchPicker onChangeComplete={(color) => this.props.setShapeColor({ id: this.props.selectedBlock.id, color: color.hex })}
                    color={(this.props.shapeColor && this.props.shapeColor.color) || '#1B7EAB'} disableAlpha />
            </div>
        )
    }
}

class Formatter extends React.Component {
    componentDidUpdate() {
        if (this.formatterRef && !this.props.formatterRef) this.props.setFormatterRef(this.formatterRef)
    }

    render() {
        const classes = this.props.classes
        return (
            <div className={classes.container} ref={ref => this.formatterRef = ref}>
                {this.props.selectedBlock && (
                    this.props.selectedBlock.type === 'text' ?
                        <TextFormatter {...this.props} />
                        :
                        <ShapeFormatter {...this.props} />
                )}
                <OutsideButtons {...this.props} />
            </div>
        )
    }
}

const styles = {
    addTextBlock: {
        right: '300px',
        top: '50px',
        position: 'fixed'
    },
    addShape: {
        right: '300px',
        top: '100px',
        position: 'fixed'
    },
    fontSizeLabel: {
        whiteSpace: 'nowrap'
    },
    container: {
        marginTop: 10,
        justifyContent: 'space-around'
    },
    button: {
        boxShadow: 'none',
        marginLeft: '5px',
        marginBottom: '5px',
        fontSize: 18,
        backgroundColor: '#909090'
    },
    fontSizeInput: {
        width: 120
    },
    formControl: {
        marginTop: 20
    },
    colorPickerContainer: {
        display: 'flex',
        justifyContent: 'center'
    }
}

const mapStateToProps = (state) => ({
    selectedBlock: state.blocks.selectedBlock,
    activeSection: state.sections.activeSection,
    shapeColor: state.blocks.shapeColor,
    formatterRef: state.blocks.formatterRef,
    inlineStyles: state.blocks.inlineStyles
})

const mapDispatchToProps = {
    toggleInlineStyle: actions.toggleInlineStyle,
    setSelectedBlock: actions.setSelectedBlock,
    setStyle: actions.setStyle,
    onAddBlock: actions.onAddBlock,
    setShapeColor: actions.setShapeColor,
    setFormatterRef: actions.setFormatterRef
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Formatter))
