import React from 'react'
import ReactDOM from 'react-dom'
import { Rnd } from 'react-rnd'
import { connect } from 'react-redux'
import { actions } from '../actions/blocks'
import EditableStyler from './EditableStyler'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

class ShapeBlock extends React.Component {
    state = {
        clicked: false,
        color: '#1B7EAB',
        x: (this.props.position && this.props.position.x) || 0,
        y: (this.props.position && this.props.position.y) || 0,
        height: (this.props.size && this.props.size.height) || '50px',
        width: (this.props.size && this.props.size.width) || '300px'
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickSomewhere)
        this.onChange()
    }

    handleClickSomewhere = (e) => {
        const selfRef = ReactDOM.findDOMNode(this.selfRef), formatterRef = ReactDOM.findDOMNode(this.props.formatterRef)
        if ((selfRef && selfRef.contains(e.target)) || (formatterRef && formatterRef.contains(e.target))) {
            // console.log('inside')
            // if (selfRef && selfRef.contains(e.target) && this.state.color !== this.props.shapeColor) this.props.setShapeColor(this.state.color)
        } else {
            this.props.onClickOutside()
            this.setState({ clicked: false })
        }
    }

    rndProps() {
        let props = {
            onClick: () => {
                this.setState({ clicked: true })
                this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'shape' })
                this.props.onBlockClick()
            },
            onDrag: () => {
                if (!this.state.clicked) {
                    this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'shape' })
                    this.setState({ clicked: true })
                }
            },
            onResize: () => {
                if (!this.state.modified) {
                    if (!this.state.clicked) {
                        this.props.setSelectedBlock({ id: this.props.uniqueId, type: 'shape' })
                        this.setState({ clicked: true })
                    }
                }
            },
            onDragStop: (e, d) => { this.setState({ x: d.x, y: d.y }, () => this.onChange()) },
            onResizeStop: (e, direction, ref, delta, position) => {
                this.setState({
                    width: ref.style.width,
                    height: ref.style.height,
                    ...position,
                }, () => this.onChange())
            }
        }
        return props

    }

    onChange = () => {
        const { color, x, y, width, height } = this.state
        let shapeState = {
            id: this.props.uniqueId,
            section: this.props.section,
            color,
            position: { x, y },
            size: { width, height }
        }
        // console.log('newData', shapeState)
    }

    componentDidUpdate() {
        if (this.props.shapeColor && this.props.shapeColor.id === this.props.uniqueId && 
            this.state.color !== this.props.shapeColor.color) {
            this.setState({ color: this.props.shapeColor.color }, () => this.onChange())
            // this.props.setShapeColor(null)
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickSomewhere)
    }

    render() {
        return (
            <Rnd
                ref={ref => this.selfRef = ref}
                tabIndex='0'
                style={styles.editor}
                size={{ width: this.state.width, height: this.state.height }}
                position={{ x: this.state.x, y: this.state.y }}
                {...this.rndProps()}
            >
                {this.state.clicked && <EditableStyler />}
                <div style={{ backgroundColor: this.state.color, width: '100%', height: '100%' }} />
            </Rnd>
        )
    }
}

const styles = {
    editor: {
        outline: 'none',
        cursor: 'move'
    }
}

const mapStateToProps = (state) => ({
    selectedBlock: state.blocks.selectedBlock,
    shapeColor: state.blocks.shapeColor,
    formatterRef: state.blocks.formatterRef
})

const mapDispatchToProps = {
    setSelectedBlock: actions.setSelectedBlock,
    setShapeColor: actions.setShapeColor
}

export default connect(mapStateToProps, mapDispatchToProps)(ShapeBlock)
