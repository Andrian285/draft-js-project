import React from 'react'
import { Paper } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import TextBlock from './TextBlock'
import { connect } from 'react-redux'
import { actions } from '../actions/blocks'
import ShapeBlock from './ShapeBlock'

const a4 = {
    width: 595,
    height: 842
}

class DocumentPage extends React.Component {
    state = {
        textBlocks: [],
        blocks: [],
        counter: 0,
        selectedBlock: null,
        defaultData: null
    }

    componentDidMount() {
        document.addEventListener('keydown', this.handleDelete)
        let content = this.props.content.find(x => x.section === this.props.data.section)
        if (this.props.content && content) {
            let textBlocks = content.textBlocks, counter = 0
            for (let block of textBlocks) {
                block.type = 'text'
                block.key = counter++
            }
            let shapes = content.shapes
            for (let shape of shapes) {
                shape.type = 'shape'
                shape.key = counter++
            }
            this.setState({ blocks: [...textBlocks, ...shapes] })
        }
    }

    addBlock = (type) => {
        let blocks = this.state.blocks
        blocks.push({ key: this.state.counter, type })
        this.setState({ blocks, counter: this.state.counter + 1 })
    }

    setSelectedBlock(key) {
        this.setState({ selectedBlock: key })
    }

    handleDelete = (e) => {
        if (e.key === 'Delete' && this.state.selectedBlock !== null && e.srcElement.contentEditable !== 'true') {
            let blocks = this.state.blocks.slice()
            blocks.splice(blocks.indexOf(blocks.find(block => block.key === this.state.selectedBlock)), 1)
            this.setState({ blocks, selectedBlock: null })
        }
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleDelete)
    }

    componentDidUpdate() {
        if (this.props.addBlock) {
            this.addBlock(this.props.addBlock.type)
            this.props.onAddBlock(null)
        }
    }

    render() {
        const { data, classes } = this.props
        return (
            <div className={classes.container} id={data.section}>
                <Paper className={classes.paper}>
                    <div className={classes.paperDataContainer}>
                        <div className={classes.sectionBlock}>{data.label.toUpperCase()}</div>
                        {this.state.blocks.map(block =>
                            <div
                                className={classes.textBlockContainer}
                                key={block.key}
                            >
                                {block.type === 'text' ?
                                    <TextBlock
                                        style={{
                                            color: 'black',
                                            fontSize: '20px'
                                        }}
                                        onClickOutside={() => this.setState({ selectedBlock: null })}
                                        onBlockClick={() => this.setState({ selectedBlock: block.key })}
                                        uniqueId={block.key}
                                        section={this.props.data.section}
                                        {...block}
                                    />
                                    :
                                    <ShapeBlock
                                        onClickOutside={() => this.setState({ selectedBlock: null })}
                                        onBlockClick={() => this.setState({ selectedBlock: block.key })}
                                        uniqueId={block.key}
                                        section={this.props.data.section}
                                        {...block}
                                    />
                                }
                            </div>
                        )}
                    </div>
                </Paper>
                <br />
            </div>
        )
    }
}

const styles = {
    container: {
        paddingBottom: 10,
        paddingTop: 10
    },
    paper: {
        width: '70vh',
        height: '99vh'
    },
    paperDataContainer: {
        height: '100%',
        display: 'flex',
        alignItems: 'center'
    },
    titleContainer: {
        marginRight: 60,
        backgroundColor: '#9DCDE3',
        height: '80%',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        fontSize: 38,
        fontWeight: 'bold',

    },
    title: {
        color: 'white',
        fontSize: '30px',
        fontWeight: 'bold',
        marginBottom: 150
    },
    infoContainer: {
        backgroundColor: '#005C87',
        width: a4.width - 30,
        height: '200px',
        position: 'absolute',
        marginTop: 100
    },
    infoMargin: {
        marginLeft: 30
    },
    clientNameContainer: {
        fontSize: '20px',
        color: '#D8AA39',
        fontWeight: 'bold',
        margin: 0,
        marginTop: 20
    },
    clientName: {
        color: '#D8AA39',
        fontSize: '20px'
    },
    userAssignedContainer: {
        fontSize: '20px',
        color: 'white',
        fontWeight: '500',
        margin: 0,
        marginTop: 10
    },
    userAssigned: {
        color: 'white',
        fontSize: '15px'
    },
    phoneEmailContainer: {
        fontSize: '15px',
        color: 'white',
        margin: 0,
        marginTop: 10
    },
    phoneEmail: {
        color: 'white',
        fontSize: '15px',
        width: '90px'
    },
    companyUrlContainer: {
        fontSize: '15px',
        color: 'white',
        margin: 0,
        marginTop: 10
    },
    companyUrl: {
        color: 'white',
        fontSize: '15px'
    },
    sectionBlock: {
        marginLeft: 0,
        marginRight: 60,
        marginTop: -600,
        backgroundColor: '#9DCDE3',
        height: '100px',
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        fontSize: '38px'
    },
    textBlockContainer: {
        display: 'flex',
        alignItems: 'center',
        position: 'absolute'
    }
}

const mapStateToProps = (state, ownProps) => {
    const data = state.blocks.addBlockData
    const addBlock = data && data.section === ownProps.data.section ? { type: data.type } : null
    return {
        addBlock
    }
}

const mapDispatchToProps = {
    onAddBlock: actions.onAddBlock
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DocumentPage))
