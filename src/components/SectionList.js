import React from 'react'
import { Typography, Divider, List, ListItem, ListItemText } from '@material-ui/core'
import { Link } from 'react-scroll'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { actions } from '../actions/sections'

function ListItemLink(props) {
  return (
    <Link spy smooth duration={500} onSetActive={(to) => props.setActiveSection(to)} to={props.to}>
      <ListItem button component='li' children={props.children} />
    </Link>
  )
}

function SectionList(props) {
  const { elms, classes } = props
  return (
    <List component='nav'>
      {elms.map(elm => (
        <div key={elm.section}>
          <ListItemLink to={elm.section} {...props}>
            <ListItemText
              disableTypography
              primary={<Typography type='body2' className={classes.labelContainer}>{elm.label}</Typography>}
            />
          </ListItemLink>
          <Divider />
        </div>
      ))}
    </List>
  )
}

const styles = {
  labelContainer: {
    textAlign: 'center',
  },
}

const mapDispatchToProps = {
  setActiveSection: actions.setActiveSection
}

export default connect(null, mapDispatchToProps)(withStyles(styles)(SectionList))
