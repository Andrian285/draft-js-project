import React from 'react'

export default (props) => (
    <div>
        <div style={{ position: 'absolute', top: '-2px', left: 0, backgroundColor: '#1B7EAB', width: '100%', height: '2px' }} />
        <div style={{ position: 'absolute', bottom: '-2px', left: 0, backgroundColor: '#1B7EAB', width: '100%', height: '2px' }} />
        <div style={{ position: 'absolute', left: '-2px', top: 0, backgroundColor: '#1B7EAB', height: '100%', width: '2px' }} />
        <div style={{ position: 'absolute', right: '-2px', top: 0, backgroundColor: '#1B7EAB', height: '100%', width: '2px' }} />
        <div style={{ position: 'absolute', top: '-6px', left: '-6px', backgroundColor: 'white', border: '1px solid grey', width: '10px', height: '10px', borderRadius: '2px' }} />
        <div style={{ position: 'absolute', top: '-6px', right: '-6px', backgroundColor: 'white', border: '1px solid grey', width: '10px', height: '10px', borderRadius: '2px' }} />
        <div style={{ position: 'absolute', bottom: '-6px', right: '-6px', backgroundColor: 'white', border: '1px solid grey', width: '10px', height: '10px', borderRadius: '2px' }} />
        <div style={{ position: 'absolute', bottom: '-6px', left: '-6px', backgroundColor: 'white', border: '1px solid grey', width: '10px', height: '10px', borderRadius: '2px' }} />
    </div>
)