import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import DocumentPage from '../components/DocumentPage'
import SectionList from '../components/SectionList'
import Formatter from '../components/Formatter'
import { templates } from '../assets/templates'

// const schema = [ //temporary
//     {
//         section: 'cover',
//         textBlocks: [
//             { id: 0, editorState: null, position: { x: 54, y: -86 }, size: { width: '300px', height: '100px' } },
//             { id: 1, editorState: null, position: { x: 262, y: 189 }, size: { width: '300px', height: '50px' } }
//         ],
//         shapes: [
//             { id: 0, color: 'black', position: { x: 260, y: -230 }, size: { width: '300px', height: '100px' } }
//         ]
//     }
// ]

class Editor extends React.Component {
    render() {
        const { classes } = this.props
        return (
            <div className={classes.container}>
                <div className={classes.sectionsContainer}>
                    <SectionList elms={templates} />
                </div>
                <div className={classes.pagesContainer}>
                    {templates.map(elm => (
                        <DocumentPage
                            data={elm}
                            key={elm.section}
                            content={[]}
                        />
                    ))}
                </div>
                <div className={classes.editorContainer}>
                    <Formatter />
                </div>
            </div>
        )
    }
}

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'row',
    },
    sectionsContainer: {
        width: '250px',
        height: '100vh',
        backgroundColor: '#EAEAEA',
        textAlign: 'center',
        borderRightStyle: 'solid',
        borderRightWidth: '2px',
        borderColor: 'gray',
        position: 'fixed',
        zIndex: 100,
    },
    pagesContainer: {
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F2F2F2',
    },
    editorContainer: {
        right: 0,
        width: '250px',
        height: '100vh',
        backgroundColor: '#EAEAEA',
        textAlign: 'center',
        borderLeftStyle: 'solid',
        borderLeftWidth: '2px',
        borderColor: 'gray',
        position: 'fixed',
        zIndex: 100,
    },
}

export default withStyles(styles)(Editor)
