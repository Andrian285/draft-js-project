import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Reducer from './reducers'
import Editor from './containers/Editor'

const store = createStore(Reducer)

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Editor />
            </Provider>
        )
    }
}