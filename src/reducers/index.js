import { combineReducers } from 'redux'
import blocks from './blocks'
import sections from './sections'

export default combineReducers({
    blocks,
    sections
})
