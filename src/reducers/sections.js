import { types } from '../actions/sections'

export default (state = [], action) => {
    switch (action.type) {
        case types.SET_ACTIVE_SECTION:
            return { ...state, activeSection: action.payload }
        default:
            return state
    }
}