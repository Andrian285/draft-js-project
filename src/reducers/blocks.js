import { types } from '../actions/blocks'

export default (state = [], action) => {
    switch (action.type) {
        case types.SET_SELECTED_BLOCK:
            return { ...state, selectedBlock: action.payload }
        case types.TOGGLE_INLINE_STYLE:
            return { ...state, inlineStyle: action.payload }
        case types.ON_ADD_BLOCK:
            return { ...state, addBlockData: action.payload }
        case types.SET_SHAPE_COLOR:
            return { ...state, shapeColor: action.payload }
        case types.SET_FORMATTER_REF:
            return { ...state, formatterRef: action.payload }
        case types.SET_INLINE_STYLES:
            return { ...state, inlineStyles: action.payload }
        default:
            return state
    }
}