import React from 'react'

export default function withImmutable(Component) {
    return class extends React.Component {
        getStateImmutable = (key) => {
            return this.copy(this.state[key])
        }

        getState = (key) => {
            return this.state[key]
        }

        copy = (o) => {
            let newO, i

            if (typeof o !== 'object') {
                return o
            }
            if (!o) {
                return o
            }

            if ('[object Array]' === Object.prototype.toString.apply(o)) {
                newO = []
                for (i = 0; i < o.length; i += 1) {
                    newO[i] = this.copy(o[i])
                }
                return newO
            }

            newO = {}
            for (i in o) {
                if (o.hasOwnProperty(i)) {
                    newO[i] = this.copy(o[i])
                }
            }
            return newO
        }

        render() {
            return (
                <Component
                    setState={(key, value) => this.setState({ [key]: value })}
                    getStateImmutable={this.getStateImmutable}
                    getState={this.getState}
                    copy={this.copy}
                    {...this.props}
                />
            )
        }
    }
}
