const defaultStyles = [
    { style: 'BOLD', icon: 'icon-bold' },
    { style: 'ITALIC', icon: 'icon-italic' },
    { style: 'UNDERLINE', icon: 'icon-underline' },
    { style: 'STRIKETHROUGH', icon: 'icon-strikethrough' },
    { style: 'CODE', icon: 'icon-embed2' }
]

let customStyles = {
    styles: [],
    styleMap: {}
}

for (let i = 6; i <= 96; i++) {
    customStyles.styles.push({ style: 'FONT_SIZE_' + i, value: i })
    customStyles.styleMap['FONT_SIZE_' + i] = { fontSize: i + 'px' }
}

export {
    defaultStyles,
    customStyles
}