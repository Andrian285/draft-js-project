export const mainPageElms = [
    {
        label: 'Cover',
        section: 'cover',
    },
    {
        label: 'Acknowledgement',
        section: 'acknowledgement',
    },
    {
        label: 'Introduction',
        section: 'introduction',
    },
    {
        label: 'Why Choose us',
        section: 'why-choose-us',
    },
    {
        label: 'Our work',
        section: 'our-work',
    },
    {
        label: 'Service Proposal',
        section: 'service-proposal',
    },
    {
        label: 'Delivery Schedule',
        section: 'delivery-schedule',
    },
    {
        label: 'Payment',
        section: 'payment',
    },
    {
        label: 'Signature',
        section: 'signature',
    },
    {
        label: 'Legal Terms',
        section: 'legal-terms',
    },
]
