import { generateActions, generateTypes } from './utils'

const typesList = [
    'SET_SELECTED_BLOCK',
    'TOGGLE_INLINE_STYLE',
    'ON_ADD_BLOCK',
    'SET_SHAPE_COLOR',
    'SET_FORMATTER_REF',
    'SET_INLINE_STYLES'
]

const types = generateTypes(typesList)
const actions = generateActions(typesList)

export {
    types,
    actions
}
