import { generateActions, generateTypes } from './utils'

const typesList = [
    'SET_ACTIVE_SECTION',
]

const types = generateTypes(typesList)
const actions = generateActions(typesList)

export {
    types,
    actions
}
