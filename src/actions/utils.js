export function multiArgsActionCreator(type, ...argNames) {
    return function (...args) {
        const action = { type }
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index]
        })
        return action
    }
}

export function defaultActionCreator(type) {
    return (payload) => ({
        type,
        payload
    })
}

export function actionNameGenerator(type) {
    return type.split('_').map((elm, indx) => indx > 0 ? elm[0] + elm.substring(1, elm.length).toLowerCase() : elm.toLowerCase()).join('')
}

export function generateTypes(strings) {
    let types = {}
    for (let type of strings) types[type] = type
    return types
}

export function generateActions(types) {
    let actions = {}
    for (let type of types) actions[actionNameGenerator(type)] = defaultActionCreator(type)
    return actions
}
